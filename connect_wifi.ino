#if defined(ESP32)
#include <WiFi.h>
#include <FirebaseESP32.h>
#elif defined(ESP8266)
#include <ESP8266WiFi.h>
#include <FirebaseESP8266.h>
#endif

#include <addons/RTDBHelper.h>

// Set these to run example.
#define FIREBASE_HOST "https://luzled-esp8-default-rtdb.firebaseio.com"
#define FIREBASE_AUTH "1UuKqIUFuOUs6F6IduqnICRRad8P4HlvEjtuNvEU"
#define WIFI_SSID "YIMILAN"
#define WIFI_PASSWORD "SASSDDSDS"

FirebaseData fbdo;
FirebaseAuth auth;
FirebaseConfig config;


unsigned long dataMillis = 0;
#define LED 14

void setup() {
  Serial.begin(9600);
  pinMode(LED, OUTPUT); 
  // connect to wifi.
  WiFi.mode(WIFI_STA);
  WiFi.begin(WIFI_SSID, WIFI_PASSWORD);
  Serial.print("Conectando a:\t");
  Serial.println(WIFI_SSID); 
  Serial.print("connecting");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print(".");
    delay(500);
  }
  Serial.println();
  Serial.print("connected: ");
  Serial.println(WiFi.localIP());
  Serial.println();

  Serial.printf("Firebase Client v%s\n\n", FIREBASE_CLIENT_VERSION);
  
  config.database_url = FIREBASE_HOST;
  config.signer.tokens.legacy_token = FIREBASE_AUTH;

  Firebase.reconnectWiFi(true);

  /* Initialize the library with the Firebase authen and config */
  Firebase.begin(&config, &auth);
  
  //Firebase.begin(FIREBASE_HOST, FIREBASE_AUTH);
}

int n = 0;

void loop() {
    if (millis() - dataMillis > 5000)
    {
        dataMillis = millis();
        Serial.printf("Led ... %s\n", Firebase.setString(fbdo, "/stateLed", "ON") ? "ON" : fbdo.errorReason().c_str());
        digitalWrite(LED, HIGH);
        delay(4000);
        Serial.printf("Led ... %s\n", Firebase.setString(fbdo, "/stateLed", "OFF") ? "OFF" : fbdo.errorReason().c_str());
        digitalWrite(LED, LOW);
        delay(4000);
    }
}
